# =========================================================================== #
# HELPERS
# =========================================================================== #

## help: print this help message
.PHONY: help
help:
	@echo 'Usage:'
	@sed -n 's/^##//p' ${MAKEFILE_LIST} | column -t -s ':' | sed -e 's/^/ /'

# =========================================================================== #
# DEVELOPMENT
# =========================================================================== #

## run: run the cmd/scan application
.PHONY: run
run:
	@go run ./cmd/scan -o reports -p ~/Developer/GitLab/gitlab-development-kit/gitlab

## run: run the cmd/scan application with go directory
.PHONY: run_go
run_go:
	@go run ./cmd/scan

## run: run the cmd/scan application with poetry directory
.PHONY: run_poetry
run_poetry:
	@go run ./cmd/scan -p ~/Developer/GitLab/ai-assist

## run: run the cmd/scan application with all dependencies
.PHONY: run_all
run_all:
	@go run ./cmd/scan -o reports -p ./internal/testdata/dep_files


# =========================================================================== #
# QUALITY CONTROL
# =========================================================================== #

## test: test the code
.PHONY: test
test:
	go test ./...

## audit: tidy dependencies, format, vet, and test the code
.PHONY: audit
audit:
	@echo 'Tidying and verifying module dependencies...'
	go mod tidy
	go mod verify
	@echo 'Formatting the code...'
	go fmt ./...
	@echo 'Vetting the code...'
	go vet ./...
	@echo 'Running tests...'
	go test -race -vet=off ./...

# =========================================================================== #
# BUILD
# =========================================================================== #

BUILDLOC ?= ./bin
APPNAME ?= x-ray-scan

current_time = $(shell date +%Y-%m-%dT%H:%M:%S%z)
git_description = $(shell git describe --always --dirty --tags --long)
linker_flags = "-s -X 'main.buildTime=${current_time}' -X 'main.build=${git_description}'"

## build: build the cmd/scan application
.PHONY: build
build:
	@echo 'Building cmd/scan...'
	go build -ldflags=${linker_flags} -o=${BUILDLOC}/${APPNAME} ./cmd/scan

# =========================================================================== #
# BUILD DOCKER
# =========================================================================== #

.PHONY: docker_build
docker_build:
	@docker build --tag x-ray-scan .

.PHONY: docker_run
docker_run:
	@docker run x-ray-scan