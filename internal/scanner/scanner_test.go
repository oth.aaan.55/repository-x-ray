package scanner

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/deps"
)

func TestScan(t *testing.T) {
	tests := []struct {
		name    string
		path    string
		depType deps.Type
		want    []deps.Dependency
		wantErr bool
	}{
		{
			"with Gemfile.lock",
			"../testdata/dep_files/Gemfile.lock",
			deps.Ruby,
			[]deps.Dependency{
				deps.NewRubyGem("attr_encrypted (~> 3.2.4)!"),
				deps.NewRubyGem("awesome_print"),
				deps.NewRubyGem("bcrypt (~> 3.1, >= 3.1.14)"),
				deps.NewRubyGem("better_errors (~> 2.10.1)"),
				deps.NewRubyGem("capybara (~> 3.39, >= 3.39.2)"),
				deps.NewRubyGem("devise (~> 4.9.3)"),
				deps.NewRubyGem("kaminari (~> 1.2.2)"),
				deps.NewRubyGem("rails (~> 7.0.8)"),
			},
			false,
		},
		{
			"without Gemfile.lock",
			"../internal/testdata/empty/Gemfile.lock",
			deps.Ruby,
			[]deps.Dependency{},
			true,
		},
		{
			"with package.json",
			"../testdata/dep_files/package.json",
			deps.JavaScript,
			[]deps.Dependency{
				deps.NewJSModule("@apollo/client", "^3.5.10"),
				deps.NewJSModule("@babel/core", "^7.18.5"),
				deps.NewJSModule("@rails/actioncable", "7.0.8"),
				deps.NewJSModule("@rails/ujs", "7.0.8"),
				deps.NewJSModule("vue", "2.7.15"),
				deps.NewJSModule("jest", "^28.1.3"),
				deps.NewJSModule("sass", "^1.69.0"),
			},
			false,
		},
		{
			"without package.json",
			"../internal/testdata/empty/package.json",
			deps.JavaScript,
			[]deps.Dependency{},
			true,
		},
		{
			"with go.mod",
			"../testdata/dep_files/go.mod",
			deps.Go,
			[]deps.Dependency{
				deps.NewGoPackage("github.com/joho/godotenv v1.5.1"),
				deps.NewGoPackage("github.com/stretchr/testify v1.8.4"),
			},
			false,
		},
		{
			"with go.mod and single package",
			"../testdata/dep_files/go1.mod",
			deps.Go,
			[]deps.Dependency{
				deps.NewGoPackage("github.com/joho/godotenv v1.5.1"),
			},
			false,
		},
		{
			"without go.mod",
			"../internal/testdata/empty/go.mod",
			deps.Go,
			[]deps.Dependency{},
			true,
		},
		{
			"with pyproject.toml",
			"../testdata/dep_files/pyproject.toml",
			deps.PythonPoetry,
			[]deps.Dependency{
				deps.NewPythonPoetryPackage(`python = "~3.9"`),
				deps.NewPythonPoetryPackage(`uvicorn = { extras = ["standard"], version = "^0.20.0" }`),
				deps.NewPythonPoetryPackage(`python-dotenv = "^0.21.0"`),
				deps.NewPythonPoetryPackage(`pytest = "^7.2.0"`),
				deps.NewPythonPoetryPackage(`flake8 = "^6.0.0"`),
			},
			false,
		},
		{
			"without pyproject.toml",
			"../internal/testdata/empty/pyproject.toml",
			deps.PythonPoetry,
			[]deps.Dependency{},
			true,
		},
		{
			"with requirements.txt",
			"../testdata/dep_files/requirements.txt",
			deps.PythonPip,
			[]deps.Dependency{
				deps.NewPythonPipPackage(`fastapi==0.104.1`),
				deps.NewPythonPipPackage(`detect-secrets>=1.4.0`),
				deps.NewPythonPipPackage(`fastapi-health!=0.3.0`),
				deps.NewPythonPipPackage(`tree-sitter~=0.20.4`),
				deps.NewPythonPipPackage(`anthropic == 0.7.7`),
				deps.NewPythonPipPackage(`uvicorn`),
				deps.NewPythonPipPackage(`python-dotenv`),
				deps.NewPythonPipPackage(`pytest==7.2.0`),
			},
			false,
		},
		{
			"without requirements.txt",
			"../internal/testdata/empty/requirments.txt",
			deps.PythonPip,
			[]deps.Dependency{},
			true,
		},
		{
			"with environment.yml",
			"../testdata/dep_files/environment.yml",
			deps.PythonConda,
			[]deps.Dependency{
				deps.NewPythonCondaPackage(`python=3.9`),
				deps.NewPythonCondaPackage(`bokeh=2.4.2`),
				deps.NewPythonCondaPackage(`conda-forge::numpy=1.21.*`),
				deps.NewPythonCondaPackage(`nodejs=16.13.*`),
				deps.NewPythonCondaPackage(`flask`),
				deps.NewPythonCondaPackage(`fastapi=0.104.1`),
				deps.NewPythonCondaPackage(`pip`),
			},
			false,
		},
		{
			"without environment.yml",
			"../internal/testdata/empty/environment.yml",
			deps.PythonConda,
			[]deps.Dependency{},
			true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Scan(tt.path, tt.depType)

			require.ElementsMatch(t, tt.want, got)
			if tt.wantErr {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
			}
		})
	}
}
