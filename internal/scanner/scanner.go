package scanner

import (
	"bufio"
	"encoding/json"
	"os"
	"strings"

	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/deps"
	"gopkg.in/yaml.v3"
)

type packageJSON struct {
	Dependencies    map[string]string `json:"dependencies"`
	DevDependencies map[string]string `json:"devDependencies"`
}

type environmentYAML struct {
	Dependencies []yaml.Node `yaml:"dependencies"`
}

func Scan(depFile string, depType deps.Type) ([]deps.Dependency, error) {
	d := []deps.Dependency{}
	file, err := os.Open(depFile)
	if err != nil {
		return d, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	switch depType {
	case deps.Ruby:
		d = scanGemfileLock(scanner)
	case deps.JavaScript:
		d, err = scanPackageJSON(file)
		if err != nil {
			return d, err
		}
	case deps.Go:
		d = scanGoMod(scanner)
	case deps.PythonPoetry:
		d = scanPythonPoetry(scanner)
	case deps.PythonPip:
		d = scanPythonPip(scanner)
	case deps.PythonConda:
		d, err = scanPythonConda(file)
		if err != nil {
			return d, err
		}
	default:
	}

	if err := scanner.Err(); err != nil {
		return d, err
	}

	return d, nil
}

func scanGemfileLock(scanner *bufio.Scanner) []deps.Dependency {
	gems := []deps.Dependency{}

	foundDependencies := false
	for scanner.Scan() {
		line := scanner.Text()

		// Looking for the DEPENDENCIES section
		if !foundDependencies && line == "DEPENDENCIES" {
			foundDependencies = true
			continue
		}

		// Stop scanning if we reached the end of the DEPENDENCIES section
		if foundDependencies && line == "" {
			break
		}

		if foundDependencies {
			gems = append(gems, deps.NewRubyGem(line))
		}
	}

	return gems
}

func scanPackageJSON(file *os.File) ([]deps.Dependency, error) {
	jsdeps := []deps.Dependency{}

	decoder := json.NewDecoder(file)

	var pj packageJSON
	err := decoder.Decode(&pj)
	if err != nil {
		return jsdeps, err
	}

	for name, version := range pj.Dependencies {
		jsdeps = append(jsdeps, deps.NewJSModule(name, version))
	}

	for name, version := range pj.DevDependencies {
		jsdeps = append(jsdeps, deps.NewJSModule(name, version))
	}

	return jsdeps, nil
}

func scanGoMod(scanner *bufio.Scanner) []deps.Dependency {
	godeps := []deps.Dependency{}

	foundDependencies := false
	for scanner.Scan() {
		line := scanner.Text()

		// Looking for the first `require` section
		if !foundDependencies && strings.HasPrefix(line, "require") {
			foundDependencies = true

			if strings.HasPrefix(line, "require (") {
				continue
			}

			// If the require section is on the same line as the `require` keyword
			// then there is only one package
			return []deps.Dependency{
				deps.NewGoPackage(strings.TrimPrefix(line, "require ")),
			}
		}

		// Stop scanning if we reached the end of the require section
		if foundDependencies && line == ")" {
			break
		}

		if foundDependencies {
			godeps = append(godeps, deps.NewGoPackage(line))
		}
	}

	return godeps
}

func scanPythonPoetry(scanner *bufio.Scanner) []deps.Dependency {
	foundDependencies := false
	pdeps := []deps.Dependency{}

	for scanner.Scan() {
		line := scanner.Text()

		depGrp := strings.HasPrefix(line, "[tool.poetry") && strings.HasSuffix(line, "dependencies]")
		if !foundDependencies && depGrp {
			foundDependencies = true
			continue
		}

		// Stop scanning the current group if we reached the end of the dependency section
		// And allow to continue scanning if there are more groups
		if foundDependencies && line == "" {
			foundDependencies = false
			continue
		}

		if foundDependencies {
			pdeps = append(pdeps, deps.NewPythonPoetryPackage(line))
		}
	}

	return pdeps
}

func scanPythonPip(scanner *bufio.Scanner) []deps.Dependency {
	pdeps := []deps.Dependency{}

	for scanner.Scan() {
		line := scanner.Text()

		if line == "" || strings.HasPrefix(line, "#") {
			continue
		}

		parts := strings.Split(line, "#")
		if len(parts) == 0 {
			continue
		}

		raw := strings.TrimSpace(parts[0])
		pdeps = append(pdeps, deps.NewPythonPipPackage(raw))
	}

	return pdeps
}

func scanPythonConda(file *os.File) ([]deps.Dependency, error) {
	pdeps := []deps.Dependency{}

	decoder := yaml.NewDecoder(file)

	var e environmentYAML
	err := decoder.Decode(&e)
	if err != nil {
		return pdeps, err
	}

	for _, node := range e.Dependencies {
		if node.ShortTag() != "!!str" {
			continue
		}

		pdeps = append(pdeps, deps.NewPythonCondaPackage(node.Value))
	}

	return pdeps, nil
}
