package discovery

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestLocateFile(t *testing.T) {
	tests := []struct {
		name     string
		dir      string
		fileName string
		wantPath string
		wantOk   bool
	}{
		{"with Gemfile.lock", "../testdata/dep_files", "Gemfile.lock", "../testdata/dep_files/Gemfile.lock", true},
		{"without Gemfile.lock", "../testdata/empty", "Gemfile.lock", "", false},
		{"with package.json", "../testdata/dep_files", "package.json", "../testdata/dep_files/package.json", true},
		{"without package.json", "../testdata/empty", "package.json", "", false},
		{"with go.mod", "../testdata/dep_files", "go.mod", "../testdata/dep_files/go.mod", true},
		{"without go.mod", "../testdata/empty", "go.mod", "", false},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotPath, gotOk := LocateFile(tt.dir, tt.fileName)

			require.Equal(t, tt.wantPath, gotPath)
			require.Equal(t, tt.wantOk, gotOk)
		})
	}
}

func TestFileChecksum(t *testing.T) {
	tests := []struct {
		name string
		path string
		want string
	}{
		{
			"with Gemfile.lock",
			"../testdata/dep_files/Gemfile.lock",
			"9d54354092027c20112dd0852cc4ded7c0713f43719eb58fc7a92342942e5437",
		},
		{
			"with package.json",
			"../testdata/dep_files/package.json",
			"26726759188d1ed8e7fcfdc72f9b4ca1b914fe10fc621cb472e529ad71f4ff79",
		},
		{
			"with no file",
			"../testdata/dep_files/no-file",
			"",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := FileChecksum(tt.path)

			require.Equal(t, tt.want, got)
		})
	}
}
