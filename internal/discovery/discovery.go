package discovery

import (
	"crypto/sha256"
	"encoding/hex"
	"os"
	"path"

	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/deps"
)

type DependencyFile struct {
	FileName  string
	Found     bool
	FoundPath string
	DirPath   string
	DepType   deps.Type
}

func LocateFile(dir string, fileName string) (string, bool) {
	filePath := path.Join(dir, fileName)

	_, err := os.Stat(filePath)
	if os.IsNotExist(err) {
		return "", false
	}

	return filePath, true
}

func FileChecksum(filePath string) string {
	data, err := os.ReadFile(filePath)
	if err != nil {
		return ""
	}

	hash := sha256.Sum256(data)

	return hex.EncodeToString(hash[:])
}
