package deps

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestTypeDescription(t *testing.T) {
	tests := []struct {
		name    string
		depType Type
		want    string
	}{
		{"Ruby", Ruby, "Ruby gems"},
		{"JavaScript", JavaScript, "JavaScript modules"},
		{"Go", Go, "Go packages"},
		{"Python Poetry", PythonPoetry, "Python packages"},
		{"Python Pip", PythonPip, "Python packages"},
		{"Python Conda", PythonConda, "Python packages"},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := TypeDescription(tt.depType)
			require.Equal(t, tt.want, got)
		})
	}
}

func TestNewRubyGem(t *testing.T) {
	tests := []struct {
		name string
		raw  string
		want Dependency
	}{
		{"valid", "rails", Dependency{Name: "rails"}},
		{"valid with version", "rails (~> 7.0.8)", Dependency{Name: "rails", Version: "(~> 7.0.8)"}},
		{"valid with versions", "capybara (~> 3.39, >= 3.39.2)", Dependency{Name: "capybara", Version: "(~> 3.39, >= 3.39.2)"}},
		{"valid with version bang", "attr_encrypted (~> 3.2.4)!", Dependency{Name: "attr_encrypted", Version: "(~> 3.2.4)!"}},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewRubyGem(tt.raw)

			require.Equal(t, tt.want.Name, got.Name)
			require.Equal(t, tt.want.Version, got.Version)
		})
	}
}

func TestNewGoPackage(t *testing.T) {
	tests := []struct {
		name string
		raw  string
		want Dependency
	}{
		{"valid", "  github.com/joho/godotenv v1.5.1", Dependency{Name: "github.com/joho/godotenv", Version: "v1.5.1"}},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewGoPackage(tt.raw)

			require.Equal(t, tt.want.Name, got.Name)
			require.Equal(t, tt.want.Version, got.Version)
		})
	}
}

func TestNewPythonPoetryPackage(t *testing.T) {
	tests := []struct {
		name string
		raw  string
		want Dependency
	}{
		{"valid", `pytest = "^7.2.0"`, Dependency{Name: "pytest", Version: "^7.2.0"}},
		{
			"valid with nested version",
			`uvicorn = { extras = ["standard"], version = "^0.20.0"`,
			Dependency{Name: "uvicorn", Version: "^0.20.0"},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewPythonPoetryPackage(tt.raw)

			require.Equal(t, tt.want.Name, got.Name)
			require.Equal(t, tt.want.Version, got.Version)
		})
	}
}

func TestNewPythonPipPackage(t *testing.T) {
	tests := []struct {
		name string
		raw  string
		want Dependency
	}{
		{"matching version", "fastapi==0.104.1", Dependency{Name: "fastapi", Version: "0.104.1"}},
		{"minimum version", "fastapi>=0.104.1", Dependency{Name: "fastapi", Version: "0.104.1"}},
		{"excluded version", "fastapi!=0.104.1", Dependency{Name: "fastapi", Version: "0.104.1"}},
		{"compatible version", "fastapi~=0.104.1", Dependency{Name: "fastapi", Version: "0.104.1"}},
		{"matching version with spaces", "fastapi == 0.104.1", Dependency{Name: "fastapi", Version: "0.104.1"}},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewPythonPipPackage(tt.raw)

			require.Equal(t, tt.want.Name, got.Name)
			require.Equal(t, tt.want.Version, got.Version)
		})
	}
}

func TestNewPythonCondaPackage(t *testing.T) {
	tests := []struct {
		name string
		raw  string
		want Dependency
	}{
		{"matching version", "fastapi=0.104.1", Dependency{Name: "fastapi", Version: "0.104.1"}},
		{"wildcard version", "fastapi=0.104.*", Dependency{Name: "fastapi", Version: "0.104.*"}},
		{"no version", "fastapi", Dependency{Name: "fastapi"}},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewPythonCondaPackage(tt.raw)

			require.Equal(t, tt.want.Name, got.Name)
			require.Equal(t, tt.want.Version, got.Version)
		})
	}
}
