package deps

import (
	"regexp"
	"strings"
)

type Type int

const (
	Ruby Type = iota
	JavaScript
	Go
	PythonPoetry
	PythonPip
	PythonConda
)

type Dependency struct {
	Name        string `json:"name"`
	Version     string `json:"version,omitempty"`
	Description string `json:"description,omitempty"`
}

func TypeDescription(t Type) string {
	switch t {
	case Ruby:
		return "Ruby gems"
	case JavaScript:
		return "JavaScript modules"
	case Go:
		return "Go packages"
	case PythonPoetry, PythonPip, PythonConda:
		return "Python packages"
	default:
		return ""
	}
}

// NewRubyGem parses a string in the format of "gem_name (version)" and returns a RubyGem struct
func NewRubyGem(raw string) Dependency {
	g := Dependency{}

	g.Name, g.Version, _ = strings.Cut(strings.TrimSpace(raw), " ")

	return g
}

func NewJSModule(name, version string) Dependency {
	return Dependency{
		Name:    name,
		Version: version,
	}
}

func NewGoPackage(raw string) Dependency {
	g := Dependency{}

	g.Name, g.Version, _ = strings.Cut(strings.TrimSpace(raw), " ")

	return g
}

func NewPythonPoetryPackage(raw string) Dependency {
	d := Dependency{}

	parts := strings.SplitN(strings.TrimSpace(raw), " = ", 2)
	d.Name = parts[0]
	if strings.HasPrefix(parts[1], `"`) {
		d.Version = strings.Trim(parts[1], `"`)
		return d
	}

	// A version could be hidden in a nested structure, such as:
	// uvicorn = { extras = ["standard"], version = "^0.20.0" }
	re := regexp.MustCompile(`version \= "(.*)"`)
	matches := re.FindStringSubmatch(parts[1])
	if len(matches) == 2 {
		d.Version = matches[1]
	}

	return d
}

func NewPythonPipPackage(raw string) Dependency {
	d := Dependency{}

	re := regexp.MustCompile(`^([a-z-_]+)\s*[!=~>]{0,1}=*\s*([0-9\.]*)`)
	matches := re.FindStringSubmatch(raw)
	if len(matches) == 3 {
		d.Name = matches[1]
		d.Version = matches[2]
	}

	return d
}

func NewPythonCondaPackage(raw string) Dependency {
	d := Dependency{}

	parts := strings.SplitN(strings.TrimSpace(raw), "=", 2)
	d.Name = parts[0]

	if len(parts) == 2 {
		d.Version = parts[1]
	}

	return d
}
