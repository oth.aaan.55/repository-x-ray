package prompt

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/deps"
)

func TestLibsDescription(t *testing.T) {
	tests := []struct {
		name    string
		libs    []deps.Dependency
		libType deps.Type
		want    string
		wantErr bool
	}{
		{"empty", []deps.Dependency{}, deps.Ruby, "", true},
		{
			"with gems",
			[]deps.Dependency{
				deps.NewRubyGem("attr_encrypted (~> 3.2.4)!"),
				deps.NewRubyGem("awesome_print"),
				deps.NewRubyGem("bcrypt (~> 3.1, >= 3.1.14)"),
				deps.NewRubyGem("better_errors (~> 2.10.1)"),
				deps.NewRubyGem("capybara (~> 3.39, >= 3.39.2)"),
				deps.NewRubyGem("devise (~> 4.9.3)"),
				deps.NewRubyGem("kaminari (~> 1.2.2)"),
				deps.NewRubyGem("rails (~> 7.0.8)"),
			},
			deps.Ruby,
			`

Human: I have the following list of Ruby gems. Could you provide a description for each of them?

The response should be in the text format with every description on a separate line.
For example:

<NAME1> --- <DESCRIPTION1>
<NAME2> --- <DESCRIPTION2>
...

Response should be enclosed in <description></description> XML tags.
Here is the list of Ruby gems in <libs></libs> XML tags:

<libs>
attr_encrypted (~> 3.2.4)!
awesome_print 
bcrypt (~> 3.1, >= 3.1.14)
better_errors (~> 2.10.1)
capybara (~> 3.39, >= 3.39.2)
devise (~> 4.9.3)
kaminari (~> 1.2.2)
rails (~> 7.0.8)
</libs>


Assistant: <description>`,
			false,
		},
		{
			"with JS modules",
			[]deps.Dependency{
				deps.NewJSModule("@apollo/client", "^3.5.10"),
				deps.NewJSModule("@babel/core", "^7.18.5"),
				deps.NewJSModule("@rails/actioncable", "7.0.8"),
				deps.NewJSModule("@rails/ujs", "7.0.8"),
				deps.NewJSModule("vue", "2.7.15"),
				deps.NewJSModule("jest", "^28.1.3"),
				deps.NewJSModule("sass", "^1.69.0"),
			},
			deps.JavaScript,
			`

Human: I have the following list of JavaScript modules. Could you provide a description for each of them?

The response should be in the text format with every description on a separate line.
For example:

<NAME1> --- <DESCRIPTION1>
<NAME2> --- <DESCRIPTION2>
...

Response should be enclosed in <description></description> XML tags.
Here is the list of JavaScript modules in <libs></libs> XML tags:

<libs>
@apollo/client ^3.5.10
@babel/core ^7.18.5
@rails/actioncable 7.0.8
@rails/ujs 7.0.8
vue 2.7.15
jest ^28.1.3
sass ^1.69.0
</libs>


Assistant: <description>`,
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := New()
			got, err := p.LibsDescription(tt.libs, tt.libType)

			if tt.wantErr {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
			}
			require.Equal(t, tt.want, got)
		})
	}
}
