package aiclient

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/deps"
)

func Test_apiEndpoint(t *testing.T) {
	gl, _ := NewGitLab("0.0.1", "http://localhost:3000/api/v4", "1", "secret-token")
	want := "http://localhost:3000/api/v4/internal/jobs/1/x_ray/scan"
	got := gl.apiEndpoint()

	require.Equal(t, want, got)
}

func TestGitLabCompletions(t *testing.T) {
	tests := []struct {
		name    string
		status  int
		resp    string
		want    []deps.Dependency
		wantErr bool
	}{
		{
			"success",
			http.StatusOK,
			`{"response": "rails --- Ruby on Rails description\nkaminari --- Kaminari description</description>"}`,
			[]deps.Dependency{
				{Name: "rails", Description: "Ruby on Rails description"},
				{Name: "kaminari", Description: "Kaminari description"},
			},
			false,
		},
		{
			"forbidden",
			http.StatusForbidden,
			`{}`,
			[]deps.Dependency{},
			true,
		},
		{
			"response is invalid JSON",
			http.StatusOK,
			`rails --- Ruby on Rails description\nkaminari --- Kaminari description</description>`,
			[]deps.Dependency{},
			true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(tt.status)
				w.Write([]byte(tt.resp))
			}))

			gl, err := NewGitLab("0.0.1", server.URL, "1", "secret-token")
			require.NoError(t, err)

			got, err := gl.Completions("prompt")
			if tt.wantErr {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
			}

			require.ElementsMatch(t, tt.want, got)
		})
	}
}
