package aiclient

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/deps"
	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/prompt"
)

type GitLab struct {
	ApiV4Url       string
	JobID          string
	Token          string
	promptType     string
	provider       string
	model          string
	scannerVersion string
}

type GitLabRequest struct {
	Token            string                         `json:"token"`
	PromptComponents []GitLabRequestPromptComponent `json:"prompt_components"`
}

type GitLabRequestPromptComponent struct {
	Type     string                               `json:"type"`
	Payload  GitLabRequestPromptComponentPayload  `json:"payload"`
	Metadata GitLabRequestPromptComponentMetadata `json:"metadata"`
}

type GitLabRequestPromptComponentPayload struct {
	Prompt   string `json:"prompt"`
	Provider string `json:"provider"`
	Model    string `json:"model"`
}

type GitLabRequestPromptComponentMetadata struct {
	ScannerVersion string `json:"scannerVersion"`
}

type GitLabResponse struct {
	Completion string `json:"response"`
}

func NewGitLab(scannerVersion, apiV4Url, jobID, token string) (*GitLab, error) {
	if apiV4Url == "" {
		return nil, fmt.Errorf("GitLab API V4 URL is missing")
	}

	if jobID == "" {
		return nil, fmt.Errorf("GitLab CI Job ID is missing")
	}

	if token == "" {
		return nil, fmt.Errorf("GitLab CI Token is missing")
	}

	return &GitLab{
		ApiV4Url:       apiV4Url,
		JobID:          jobID,
		Token:          token,
		promptType:     "x_ray_package_file_prompt",
		provider:       "anthropic",
		model:          "claude-2.0",
		scannerVersion: scannerVersion,
	}, nil
}

func (gl *GitLab) apiEndpoint() string {
	return fmt.Sprintf("%s/internal/jobs/%s/x_ray/scan", gl.ApiV4Url, gl.JobID)
}

func (gl *GitLab) newGitLabRequest(prompt string) *GitLabRequest {
	return &GitLabRequest{
		Token: gl.Token,
		PromptComponents: []GitLabRequestPromptComponent{
			{
				Type: gl.promptType,
				Payload: GitLabRequestPromptComponentPayload{
					Prompt:   prompt,
					Provider: gl.provider,
					Model:    gl.model,
				},
				Metadata: GitLabRequestPromptComponentMetadata{
					ScannerVersion: gl.scannerVersion,
				},
			},
		},
	}
}

func (gl *GitLab) Completions(prompt string) ([]deps.Dependency, error) {
	r := []deps.Dependency{}
	client := &http.Client{}
	data := gl.newGitLabRequest(prompt)
	jsonData, err := json.Marshal(data)
	if err != nil {
		return r, err
	}

	req, err := http.NewRequest("POST", gl.apiEndpoint(), bytes.NewBuffer(jsonData))
	if err != nil {
		return r, err
	}

	req.Header.Set("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		return r, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return r, fmt.Errorf("GitLab API returned non-200 status code: %d", resp.StatusCode)
	}

	var glr GitLabResponse
	err = json.NewDecoder(resp.Body).Decode(&glr)
	if err != nil {
		return nil, fmt.Errorf("Failed to decode response body: %v", err)
	}

	r = parseBody(glr.String())

	return r, nil
}

func (glr *GitLabResponse) String() string {
	return strings.TrimSuffix(glr.Completion, prompt.ResponseClosingTag)
}
