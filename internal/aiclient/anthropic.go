package aiclient

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/deps"
	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/prompt"
)

type Anthropic struct {
	APIKey            string
	Model             string
	MaxTokensToSample int
	Version           string
	APIEndpoint       string
}

type AnthropicResponse struct {
	Completion string `json:"completion"`
	StopReason string `json:"stop_reason"`
}

func NewAnthropic(apiKey string) *Anthropic {
	return &Anthropic{
		APIKey:            apiKey,
		Model:             "claude-2.1",
		MaxTokensToSample: 4096,
		Version:           "2023-06-01",
		APIEndpoint:       "https://api.anthropic.com/v1/complete",
	}
}

func (a *Anthropic) Completions(prompt string) ([]deps.Dependency, error) {
	r := []deps.Dependency{}
	client := &http.Client{}
	data := map[string]interface{}{
		"prompt":               prompt,
		"model":                a.Model,
		"max_tokens_to_sample": a.MaxTokensToSample,
	}
	jsonData, err := json.Marshal(data)
	if err != nil {
		return r, err
	}

	req, err := http.NewRequest("POST", a.APIEndpoint, bytes.NewBuffer(jsonData))
	if err != nil {
		return r, err
	}

	req.Header.Set("Anthropic-Version", a.Version)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-API-Key", a.APIKey)

	resp, err := client.Do(req)
	if err != nil {
		return r, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return r, fmt.Errorf("Anthropic API returned non-200 status code: %d", resp.StatusCode)
	}

	var ar AnthropicResponse
	err = json.NewDecoder(resp.Body).Decode(&ar)
	if err != nil {
		return r, fmt.Errorf("Failed to decode Anthropic response body: %v", err)
	}

	if ar.ReachedMaxTokens() {
		return r, errors.New("Anthropic API returned max tokens error")
	}

	r = parseBody(ar.String())

	return r, nil
}

func (ar *AnthropicResponse) String() string {
	return strings.TrimSuffix(ar.Completion, prompt.ResponseClosingTag)
}

func (ar *AnthropicResponse) ReachedMaxTokens() bool {
	return ar.StopReason == "max_tokens"
}
