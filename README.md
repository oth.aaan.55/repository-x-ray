# Repository X-Ray

## How to run locally

1. Make sure `gdk` working locally
1. Prepare a job on localhost
    1. Run `bundle exec rails c` from your `gdk/rails` directory
    1. Run the following Ruby code:
        ```ruby
        build = Ci::Build.first
        build.set_token 'secret'
        build.status = "running"
        build.save
        namespace = build.project.namespace
        FactoryBot.create(:gitlab_subscription_add_on_purchase, add_on: FactoryBot.create(:gitlab_subscription_add_on), namespace: namespace)
        build.id
        ```
    1. Exit the console `exit`
    1. Optionally you can check if everything is working on the Rails side:
        ```
        curl -v -X POST -H "Content-Type: Application/json"  http://localhost:3000/api/v4/internal/jobs/1/x_ray/scan --data '{
          "token": "secret",
          "prompt_components": [
            {
              "type":"x_ray_package_file_prompt",
              "payload":{
                "prompt": "\n\nHuman: What is your model name? \n\n Assistant:",
                "provider": "anthropic",
                "model": "claude-2.0"
              },
              "metadata": { "scannerVersion": "0.0.1" }
            }
          ]
        }'
        ```

        you should see a response similar to

        ```
        {"response":" I don't have a specific model name. I'm Claude, an AI assistant created by Anthropic."}
        ```
1. Run `cp .env.example .env` in the X-Ray directory
1. Set the required variables in `.env`
    1. Set `CI_API_V4_URL` to your localhost url
    1. Set `CI_JOB_ID` to `build.id` from previous steps
    1. Set `CI_JOB_TOKEN` to `secret` (or to a token used in `build.set_token 'secret'` above)

## Dogfooding

This repository itself uses X-Ray scan feature by defining `xray_scan` job in its `.gitlab-ci.yml` config file. It is possible to preview raw content of X-Ray scan report by viewing `xray_scan` job attached artifacts. You can read more about `artifacts: reports` and CI job configuration at [dedicated GitLab documentation page](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsrepository_xray).

## Release Process

In order to provide more control over the release process that distributes changes to the X-Ray scanner to GitLab users, this repository follows the release process defined below:

1. On feature branches, `build` CI jobs create `registry.gitlab.com/gitlab-org/code-creation/repository-x-ray/dev:$CI_COMMIT_SHORT_SHA` images that are used for end-to-end testing purposes.

2. To release a new version of the scanner, a new [Git tag](https://docs.gitlab.com/ee/user/project/repository/tags/#create-a-tag) must be created.

3. The `build` CI job that runs on Git tags creates the `registry.gitlab.com/gitlab-org/code-creation/repository-x-ray` image and tags it as `rc`.

4. Images with the `rc` tag are used by [`gitlab-org/gitlab`](https://gitlab.com/gitlab-org/gitlab) and this project for testing. 

5. After one business day of using the new `rc` image, a manual `release` job is created that tags the `rc` image as `latest` to make it available to all GitLab users.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
