package main

import (
	"flag"
	"fmt"
	"log/slog"
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/aiclient"
	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/deps"
	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/discovery"
	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/prompt"
	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/report"
	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/scanner"
	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/utils"
)

const (
	version            = "0.0.1"
	fetchDescBatchSize = 25
)

var (
	build     string
	buildTime string

	dependencies = []*discovery.DependencyFile{
		{FileName: "go.mod", DepType: deps.Go},
		{FileName: "package.json", DepType: deps.JavaScript},
		{FileName: "Gemfile.lock", DepType: deps.Ruby},
		{FileName: "pyproject.toml", DepType: deps.PythonPoetry},
		{FileName: "requirements.txt", DepType: deps.PythonPip},
		{FileName: "environment.yml", DepType: deps.PythonConda},
	}

	dstFiles = map[deps.Type]string{
		deps.Go:           "go.json",
		deps.JavaScript:   "javascript.json",
		deps.Ruby:         "ruby.json",
		deps.PythonPoetry: "python.json",
		deps.PythonPip:    "python.json",
		deps.PythonConda:  "python.json",
	}
)

type appMetadata struct {
	buildTime string
	version   string
}

func main() {
	godotenv.Load()

	dir, err := os.Getwd()
	if err != nil {
		slog.Error("Failed to get current working directory", err)
		return
	}
	scanDir := flag.String("p", dir, "Path to the directory to scan")
	reportsDir := flag.String("o", "reports", "Path to the directory to store scan reports")
	displayVersion := flag.Bool("version", false, "Print app version")
	flag.Parse()

	metadata := appMetadata{
		buildTime: buildTime,
		version:   fmt.Sprintf("%s+%s", version, build),
	}

	if *displayVersion {
		printVersionInfo(metadata)
		os.Exit(0)
	}

	slog.Info("Scanning in " + *scanDir)

	for _, dep := range dependencies {
		dep.FoundPath, dep.Found = discovery.LocateFile(*scanDir, dep.FileName)
		dep.DirPath = *scanDir
		if dep.Found {
			slog.Info(fmt.Sprintf("Found %s at %s", dep.FileName, dep.FoundPath))
		}
	}

	for _, dep := range dependencies {
		if !dep.Found {
			continue
		}

		checksum := discovery.FileChecksum(dep.FoundPath)
		r := report.New(*reportsDir, dstFiles[dep.DepType], version, dep.FileName, checksum)

		processDependencies(*dep, r)
	}

	slog.Info("X-Ray scan finished")
}

func processDependencies(dep discovery.DependencyFile, rprt *report.Report) {
	slog.Info(fmt.Sprintf("Scanning %s", dep.FileName))
	dd := deps.TypeDescription(dep.DepType)
	foundDeps, _ := scanner.Scan(dep.FoundPath, dep.DepType)
	slog.Info(fmt.Sprintf("Found %d %s", len(foundDeps), dd))
	if len(foundDeps) == 0 {
		return
	}

	p := prompt.New()
	client, err := aiclient.NewGitLab(
		rprt.ScannerVersion,
		os.Getenv("CI_API_V4_URL"),
		os.Getenv("CI_JOB_ID"),
		os.Getenv("CI_JOB_TOKEN"),
	)
	if err != nil {
		slog.Error(fmt.Sprintf("Failed to create GitLab client: %v", err))
		return
	}

	slog.Info(fmt.Sprintf("Getting %s descriptions. This can take a while...\n", dd))

	rprt.Libs = make([]deps.Dependency, 0, len(foundDeps))
	utils.EachBatch(foundDeps, fetchDescBatchSize, func(batch []deps.Dependency, totalBatches int, currentBatch int) {
		slog.Info(fmt.Sprintf("Processing batch... (%d/%d)\n", currentBatch, totalBatches))

		prmt, err := p.LibsDescription(batch, dep.DepType)
		if err != nil {
			slog.Warn(fmt.Sprintf("failed to prepare prompt for %s: %v", deps.TypeDescription(dep.DepType), err))
			return
		}

		resp, err := client.Completions(prmt)
		if err != nil {
			slog.Warn(fmt.Sprintf("Failed to get completions. Skipping batch. %v\n", err))
			return
		}

		rprt.Libs = append(rprt.Libs, resp...)
	})

	err = rprt.Save()
	if err != nil {
		slog.Error(fmt.Sprintf("Failed to save report: %v", err))
		return
	}
}

func printVersionInfo(metadata appMetadata) {
	fmt.Printf("Version: %s\n", metadata.version)
	fmt.Printf("Build time: %s\n", metadata.buildTime)
}
